package com.atabs.recipeapp.uitls

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import androidx.annotation.RequiresApi
import java.io.ByteArrayOutputStream
import java.util.Base64

class BitmapConverter {

    companion object{
        @RequiresApi(Build.VERSION_CODES.O)
        fun bitmapToString(bitmap: Bitmap) : String{
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 10, baos)
            val imageBytes = baos.toByteArray()
            //Base64.encodeToString(data, Base64.DEFAULT)
            return Base64.getEncoder().encodeToString(imageBytes)
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun stringToBitmap(data: String) : Bitmap {

            val imageBytes = Base64.getDecoder().decode(data)
            return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        }

    }

}