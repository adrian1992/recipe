package com.atabs.recipeapp.repository

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.atabs.recipeapp.database.data.Category
import com.atabs.recipeapp.database.data.CategoryWithRecipes
import com.atabs.recipeapp.database.data.Recipe
import com.atabs.recipeapp.database.data.RecipesDao
import io.reactivex.Single

class RecipeRepository(private val recipesDao: RecipesDao){


    var allRecipes : LiveData<CategoryWithRecipes> = recipesDao.getCategoryWithRecipes()
    val allCategory : LiveData<List<Category>> = recipesDao.getCategories()

    suspend fun insert(recipe: Recipe){
        recipesDao.insertRecipe(recipe)
    }

    fun getRecipe(recipeId: Int) : Single<Recipe> {
        return recipesDao.getRecipe(recipeId)
    }

    fun getRecipeByCategory(categoryId: Int) : Single<CategoryWithRecipes> {
        return recipesDao.getCategoryWithRecipes(categoryId)
    }


}