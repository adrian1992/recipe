package com.atabs.recipeapp.database.converter

import androidx.room.TypeConverter
import com.atabs.recipeapp.database.data.ListStringObj
import com.google.gson.Gson


class ListStringObjConverter {

    @TypeConverter
    fun storedStringToIngredients(value: String) : ListStringObj{
        val ingredients = value.split(",")
        return ListStringObj(ingredients)
    }

    @TypeConverter
    fun ingredientsToStoredString(listStringObj: ListStringObj) : String{

        return listStringObj.ingList.joinToString(",")
    }

}