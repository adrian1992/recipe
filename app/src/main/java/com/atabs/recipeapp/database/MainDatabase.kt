package com.atabs.recipeapp.database

import android.content.Context
import android.util.Log
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import com.atabs.recipeapp.database.converter.ListStringObjConverter
import com.atabs.recipeapp.database.data.*
import com.atabs.recipeapp.uitls.BitmapConverter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(version = 1, entities = [Category::class, Recipe::class])
@TypeConverters(ListStringObjConverter::class)
abstract class MainDatabase : RoomDatabase(){

    abstract fun getRecipeCategoryDao(): RecipesDao

    private class MainDatabaseCallback(private val scope: CoroutineScope) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let {database ->
                scope.launch {
                    val recipesDao = database.getRecipeCategoryDao()

                    val category = Category(categoryName = "Breakfast", categoryDesc = "Breakfast Recipe")
                    val category2 = Category(categoryName = "Asian Cuisine", categoryDesc = "Breakfast Recipe")
                    val category3 = Category(categoryName = "Western", categoryDesc = "Breakfast Recipe")

                    val categoryId = recipesDao.insertCategory(category)
                    val categoryId2 = recipesDao.insertCategory(category2)
                    val categoryId3 = recipesDao.insertCategory(category3)

                    val listStringObj = ListStringObj(listOf("test", "test", "test"))


                    val recipe = Recipe(recipeCategoryId = categoryId.toInt(),
                        name = "Egg",
                        description = "testing",
                        ingredientList = listStringObj,
                        stepList = listStringObj)
                    recipesDao.insertRecipe(recipe)

                    val recipe2 = Recipe(recipeCategoryId = categoryId.toInt(),
                        name = "Bulalo",
                        description = "testing",
                        ingredientList = listStringObj,
                        stepList = listStringObj)

                    recipesDao.insertRecipe(recipe2)

                    val recipe3 = Recipe(recipeCategoryId = categoryId2.toInt(),
                        name = "Pho",
                        description = "Description placeholder",
                        ingredientList = listStringObj,
                        stepList = listStringObj)

                    recipesDao.insertRecipe(recipe3)

                    val recipe4 = Recipe(recipeCategoryId = categoryId3.toInt(),
                        name = "Burger",
                        description = "Description placeholder",
                        ingredientList = listStringObj,
                        stepList = listStringObj)

                    recipesDao.insertRecipe(recipe4)

                    Log.d("DB","Success preloading data")

                }
            }
        }

    }

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE : MainDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): MainDatabase {
            val tempInstance = INSTANCE
            if(tempInstance != null) {
                return tempInstance
            }

            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MainDatabase::class.java,
                    "main_database"
                )
                    .allowMainThreadQueries()
                    .addCallback(MainDatabaseCallback(scope)).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}