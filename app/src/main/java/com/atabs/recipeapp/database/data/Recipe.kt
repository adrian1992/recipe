package com.atabs.recipeapp.database.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import io.reactivex.Single

@Entity
data class Category(
    @PrimaryKey(autoGenerate = true) @ColumnInfo val categoryId: Int? = null,
    @ColumnInfo val categoryName: String,
    @ColumnInfo val categoryDesc: String
)

@Entity
data class Recipe(
    @PrimaryKey(autoGenerate = true) @ColumnInfo val recipeId: Int? = null,
    @ColumnInfo val recipeCategoryId: Int,
    @ColumnInfo val name: String,
    @ColumnInfo val description: String,
    @ColumnInfo val ingredientList: ListStringObj,
    @ColumnInfo val stepList: ListStringObj,
    @ColumnInfo val image: String? = null
)

data class CategoryWithRecipes(
    @Embedded val category: Category,
    @Relation(
        parentColumn = "categoryId",
        entityColumn = "recipeCategoryId"
    )
    val recipes: List<Recipe>
)

data class ListStringObj(
    val ingList: List<String>
)
@Dao
interface RecipesDao {

    @Transaction
    @Query("Select * from Category")
    fun getCategoryWithRecipes(): LiveData<CategoryWithRecipes>

    @Transaction
    @Query("Select * from Category where categoryId = :categoryId ")
    fun getCategoryWithRecipes(categoryId: Int) : Single<CategoryWithRecipes>

    @Query("Select * from Category")
    fun getCategories() : LiveData<List<Category>>

    @Query("Select * from Recipe where recipeId = :recipeId")
    fun getRecipe(recipeId: Int) : Single<Recipe>

    @Query("Select * from Recipe where recipeCategoryId =:categoryId")
    fun getRecipeByCategory(categoryId: Int) : LiveData<List<Recipe>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRecipe(recipe: Recipe) : Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategory(category: Category) : Long


}