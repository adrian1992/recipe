package com.atabs.recipeapp.fragments.recipe_list

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.atabs.recipeapp.R
import com.atabs.recipeapp.uitls.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class RecipeListFragment : Fragment() {

    lateinit var recipeListView: RecipeListView
    private lateinit var recipeListViewModel: RecipeListViewModel
    private lateinit var compositeDisposable: CompositeDisposable

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        recipeListView = RecipeListView()

        return recipeListView.createView(inflater, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        compositeDisposable = CompositeDisposable()

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recipeListViewModel = ViewModelProvider(this).get(RecipeListViewModel::class.java)



        recipeListView.setupUi()

        setUpEvent()

    }

    fun setUpEvent(){

        compositeDisposable += recipeListView.getFabClick().observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                findNavController().navigate(R.id.action_recipeList_to_AddRecipeFragment)
            }, {
                Log.d("Error", it.localizedMessage)
            })

        compositeDisposable += recipeListView.getOnItemClick().observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                findNavController().navigate(RecipeListFragmentDirections
                    .actionRecipeListToRecipeDetailFragment(recipeData = it))
            },{
                Log.d("Error", it.localizedMessage)
            })

        compositeDisposable += recipeListView.getSpinnerItemSelected()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it?.let {
                    recipeListViewModel.getRecipeByCategory(it)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({data->
                            recipeListView.updateControllerData(data.recipes)
                        },{
                            Log.e("Error", it.localizedMessage)
                        })
                    Log.d("spinner", "$it")
                }
            },{
                    Log.e("Error", it.localizedMessage)
            })

        recipeListViewModel.allRecipe.observe(viewLifecycleOwner, Observer {
            if(it != null) {
                Log.d("DB", "recipe size ${it.recipes.size}")
                if(it.recipes.isNotEmpty()){
                    recipeListView.initializeController(it.recipes)
                }
            }
        })

        recipeListViewModel.allCategory.observe(viewLifecycleOwner, Observer {
            it?.let {list->
                val map = HashMap<String, Int>()
                val stringList = mutableListOf<String>()
                list.forEach {cat->
                    stringList.add(cat.categoryName)
                    map[cat.categoryName] = cat.categoryId?.let {id->
                        id
                    } ?: kotlin.run {
                        1
                    }
                }
                context?.let {context->
                    recipeListView.setUpSpinner(stringList, map, context)
                }
            }
        })
    }

    override fun onDestroyView() {
        if (::compositeDisposable.isInitialized &&
            !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
        super.onDestroyView()
    }
}

