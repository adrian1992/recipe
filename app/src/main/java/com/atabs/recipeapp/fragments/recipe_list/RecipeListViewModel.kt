package com.atabs.recipeapp.fragments.recipe_list

import android.app.Application
import android.widget.ListView
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.atabs.recipeapp.database.MainDatabase
import com.atabs.recipeapp.database.data.Category
import com.atabs.recipeapp.database.data.CategoryWithRecipes
import com.atabs.recipeapp.database.data.Recipe
import com.atabs.recipeapp.database.data.RecipesDao
import com.atabs.recipeapp.repository.RecipeRepository
import io.reactivex.Single
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class RecipeListViewModel(application: Application) : AndroidViewModel(application) {

    private val recipeRepository : RecipeRepository
    val allRecipe: LiveData<CategoryWithRecipes>
    val allCategory: LiveData<List<Category>>

    init {
        val recipesDao = MainDatabase.getDatabase(application, viewModelScope).getRecipeCategoryDao()
        recipeRepository = RecipeRepository(recipesDao)
        allRecipe = recipeRepository.allRecipes
        allCategory = recipeRepository.allCategory
    }

    fun insert(recipe: Recipe) = viewModelScope.launch {
        recipeRepository.insert(recipe)
    }

    fun getRecipeByCategory(categoryId: Int) : Single<CategoryWithRecipes> {
        return recipeRepository.getRecipeByCategory(categoryId)
    }

}