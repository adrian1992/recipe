package com.atabs.recipeapp.fragments.recipe_details

import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.widget.NestedScrollView
import androidx.navigation.findNavController
import com.atabs.recipeapp.database.data.ListStringObj
import com.atabs.recipeapp.database.data.Recipe
import com.atabs.recipeapp.databinding.FragmentRecipeDetailsBinding
import com.atabs.recipeapp.uitls.BitmapConverter
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RecipeDetailsView {

    private lateinit var binding: FragmentRecipeDetailsBinding
    private var editEvent = PublishSubject.create<Unit>()

    fun onCreateView(inflater: LayoutInflater, container: ViewGroup?) : View {
        binding = FragmentRecipeDetailsBinding.inflate(inflater, container, false)

        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setupUi(recipe: Recipe){
        var isToolbarShown = false

        binding.plantDetailScrollview.setOnScrollChangeListener(
            NestedScrollView.OnScrollChangeListener { _, _, scrollY, _, _ ->

                // User scrolled past image to height of toolbar and the title text is
                // underneath the toolbar, so the toolbar should be shown.
                val shouldShowToolbar = scrollY > binding.toolbar.height

                // The new state of the toolbar differs from the previous state; update
                // appbar and toolbar attributes.
                if (isToolbarShown != shouldShowToolbar) {
                    isToolbarShown = shouldShowToolbar

                    // Use shadow animator to add elevation if toolbar is shown
                    binding.appbar.isActivated = shouldShowToolbar

                    // Show the plant name if toolbar is shown
                    binding.toolbarLayout.isTitleEnabled = shouldShowToolbar
                }
            }
        )

        binding.toolbar.setNavigationOnClickListener { view ->
            view.findNavController().navigateUp()
        }
        binding.plantDetailName.text = recipe.name
        binding.plantDescription.text = recipe.description
            .plus(getIngredientsFormatted(recipe.ingredientList, "Ingredients"))
            .plus(getIngredientsFormatted(recipe.stepList, "Steps"))
        binding.toolbarLayout.title = recipe.name

        recipe.image?.let {
            val bitmap = BitmapConverter.stringToBitmap(it)
            binding.detailImage.setImageBitmap(bitmap)
        }

        binding.fab.setOnClickListener {
            editEvent.onNext(Unit)
            Log.d("Click", "click")
        }

    }

    private fun getIngredientsFormatted(listObj: ListStringObj, title: String) : String{
        var formatted = "\n\n$title: \n\n";

        listObj.ingList.forEach {
            formatted += "\u2022 $it \n"
        }

        return formatted
    }

    fun getEditEvent() : Observable<Unit> {
        return editEvent
    }
}