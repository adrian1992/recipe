package com.atabs.recipeapp.fragments.add_recipe

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.atabs.recipeapp.uitls.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable


class AddRecipeFragment : Fragment() {

    private lateinit var addRecipeView : AddRecipeView
    private lateinit var viewModel: AddRecipeViewModel
    private lateinit var compositeDisposable: CompositeDisposable
    private val args : AddRecipeFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        addRecipeView = AddRecipeView()

        return addRecipeView.onCreateView(inflater,container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AddRecipeViewModel::class.java)
        compositeDisposable = CompositeDisposable()

        viewModel.allCategory.observe(viewLifecycleOwner, Observer {categories ->
            context?.let {

                val listCategories = mutableListOf<String>()
                val map = HashMap<String, Int>()

                categories.forEach {cat ->
                    listCategories.add(cat.categoryName)
                    map[cat.categoryName] = cat.categoryId?.let {id ->
                        id
                    } ?: kotlin.run {
                        1
                    }
                }

                addRecipeView.initUi(it, listCategories, map)
            }
        })

        compositeDisposable += viewModel.getRecipe(args.recipeData).subscribe({

            addRecipeView.displayData(it)

        },{

        })

        setUpEvent()
    }

    fun setUpEvent(){

        compositeDisposable += addRecipeView.getSubmitEvent()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                viewModel.insert(it)
                findNavController().navigateUp()
            },{
                Log.e("Error", it.localizedMessage)
            })

        compositeDisposable += addRecipeView.getAddEventImg()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val photoPickerIntent = Intent(Intent.ACTION_GET_CONTENT)
                photoPickerIntent.type = "image/*"
                startActivityForResult(photoPickerIntent, 1)
            },{
                Log.e("Error", it.localizedMessage)
            })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK){
            val chosenImageUri = data!!.data

            var bitmap = MediaStore.Images.Media.getBitmap(context!!.contentResolver, chosenImageUri)
            addRecipeView.imageSelected(true, bitmap)
        }
    }

    override fun onDestroyView() {
        if (::compositeDisposable.isInitialized &&
            !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
        super.onDestroyView()
    }

}