package com.atabs.recipeapp.fragments.add_recipe

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.atabs.recipeapp.database.MainDatabase
import com.atabs.recipeapp.database.data.Category
import com.atabs.recipeapp.database.data.CategoryWithRecipes
import com.atabs.recipeapp.database.data.Recipe
import com.atabs.recipeapp.repository.RecipeRepository
import io.reactivex.Single
import kotlinx.coroutines.launch

class AddRecipeViewModel(application: Application) : AndroidViewModel(application){


    private val recipeRepository : RecipeRepository
    val allCategory: LiveData<List<Category>>

    init {
        val recipesDao = MainDatabase.getDatabase(application, viewModelScope).getRecipeCategoryDao()
        recipeRepository = RecipeRepository(recipesDao)
        allCategory = recipeRepository.allCategory
    }

    fun insert(recipe: Recipe) = viewModelScope.launch {
        recipeRepository.insert(recipe)
    }

    fun getRecipe(recipeId: Int) : Single<Recipe> {
        return recipeRepository.getRecipe(recipeId)
    }
}