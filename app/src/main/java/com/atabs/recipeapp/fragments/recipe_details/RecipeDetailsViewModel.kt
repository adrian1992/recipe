package com.atabs.recipeapp.fragments.recipe_details

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.atabs.recipeapp.database.MainDatabase
import com.atabs.recipeapp.database.data.Recipe
import com.atabs.recipeapp.repository.RecipeRepository
import io.reactivex.Single

class RecipeDetailsViewModel(application: Application) : AndroidViewModel(application){

    private val recipeRepository : RecipeRepository

    init {
        val recipesDao = MainDatabase.getDatabase(application, viewModelScope).getRecipeCategoryDao()
        recipeRepository = RecipeRepository(recipesDao)
    }

    fun getRecipe(recipeId: Int) : Single<Recipe> {
        return recipeRepository.getRecipe(recipeId)
    }

}