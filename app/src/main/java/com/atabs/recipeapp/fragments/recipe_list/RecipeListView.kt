package com.atabs.recipeapp.fragments.recipe_list


import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyVisibilityTracker
import com.atabs.recipeapp.R
import com.atabs.recipeapp.database.data.Recipe
import com.atabs.recipeapp.databinding.FragmentRecipeListBinding
import com.atabs.recipeapp.fragments.recipe_list.epoxy_controller.RecipeListController
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.w3c.dom.Text


class RecipeListView {

    private lateinit var binding: FragmentRecipeListBinding
    private lateinit var controller: RecipeListController
    private  var fabClickEvent = PublishSubject.create<Unit>()
    private var onItemClickEvent = PublishSubject.create<Int>()
    private var onSpinnerItemClickEvent = PublishSubject.create<Int>()

    fun createView(inflater: LayoutInflater, container: ViewGroup?) : View {
        binding = FragmentRecipeListBinding.inflate(inflater, container, false)

        controller = RecipeListController()


        return binding.root
    }

    fun setUpSpinner(categories: List<String>, mapCatId: HashMap<String, Int>, context: Context){
        val adapter = ArrayAdapter<String>(context, R.layout.spinner_item, categories)

        binding.categorySpinner.adapter = adapter

        binding.categorySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(view is TextView){
                    val tv = view as TextView
                    val catId = mapCatId[tv.text]
                    Log.d("spinner", "textview $id")
                    catId?.let {
                        onSpinnerItemClickEvent.onNext(it)
                    }
                }
            }

        }
    }

    fun setupUi(){

        binding.fab.setOnClickListener {
            fabClickEvent.onNext(Unit)
        }
    }

    fun getSpinnerItemSelected() : Observable<Int> {
        return onSpinnerItemClickEvent
    }

    fun getFabClick() : Observable<Unit> {
        return fabClickEvent
    }

    fun getOnItemClick() : Observable<Int> {
        return onItemClickEvent
    }

    fun updateControllerData(recipes: List<Recipe>){
        controller.setData(recipes)
    }

    fun initializeController(recipes: List<Recipe>) {

        val epoxyVisibilityTracker = EpoxyVisibilityTracker()
        epoxyVisibilityTracker.attach(binding.recipeListEpoxyRv)
        controller.setData(recipes)
        controller.setOnItemClicked {
            onItemClickEvent.onNext(it)
        }

        binding.recipeListEpoxyRv.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            setHasFixedSize(true)
            adapter = controller.adapter
        }


    }

}
