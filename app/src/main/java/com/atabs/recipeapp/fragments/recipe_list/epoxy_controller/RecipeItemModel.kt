package com.atabs.recipeapp.fragments.recipe_list.epoxy_controller

import android.os.Build
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.atabs.recipeapp.R
import com.atabs.recipeapp.uitls.BitmapConverter
import com.atabs.recipeapp.uitls.KotlinHolder
import com.bumptech.glide.Glide

@EpoxyModelClass(layout = R.layout.component_recipe_card_item)
abstract class RecipeItemModel(@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
                               var onItemClicked: (Int) -> Unit,
                               @EpoxyAttribute var image : String = "",
                               @EpoxyAttribute var title : String = "",
                               @EpoxyAttribute var description: String = "",
                               @EpoxyAttribute var recipeId: Int = 0,
                               @EpoxyAttribute var index: Int = 0) : EpoxyModelWithHolder<RecipeItemModel.Holder>() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun bind(holder: Holder) {
        holder.titleTv.text = title
        holder.description.text = description
        if(index == 0){
            holder.vLineHolder.visibility = View.GONE
        }else{
            holder.vLineHolder.visibility = View.VISIBLE
        }

        holder.recipeItemParent.setOnClickListener {
            onItemClicked(recipeId)
            Log.d("Click", "model click")
        }

        if(image.isNotEmpty()){
            val bitmap = BitmapConverter.stringToBitmap(image)

            //holder.thumbnailImg.setImageBitmap(bitmap)
            Glide.with(holder.thumbnailImg.context)
                .load(bitmap).into(holder.thumbnailImg)
        }else{
            holder.thumbnailImg.setImageResource(R.drawable.asian)
        }




    }

    inner class Holder : KotlinHolder(){
        val thumbnailImg by bind<ImageView>(R.id.recipeThumbNail)
        val titleTv by bind<TextView>(R.id.recipeNameTv)
        val vLineHolder by bind<View>(R.id.hLineView)
        val description by bind<TextView>(R.id.recipeDescriptionTv)
        val recipeItemParent by bind<ConstraintLayout>(R.id.recipeItemParent)
    }
}