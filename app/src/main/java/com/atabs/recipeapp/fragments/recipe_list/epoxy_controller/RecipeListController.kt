package com.atabs.recipeapp.fragments.recipe_list.epoxy_controller

import com.airbnb.epoxy.TypedEpoxyController
import com.atabs.recipeapp.database.data.Recipe


class RecipeListController : TypedEpoxyController<List<Recipe>>(){

    private var onItemClicked: (Int) -> Unit = {}

    fun setOnItemClicked(onItemClicked: (Int) -> Unit) {
        this.onItemClicked = onItemClicked
    }

    override fun buildModels(data: List<Recipe>?) {
        data?.let {
            it.forEachIndexed { index, recipe ->
                val image = recipe.image?.let {
                    it
                } ?: kotlin.run {
                    ""
                }
                RecipeItemModel_(onItemClicked, image, recipe.name, recipe.description, recipe.recipeId ?: 0, index).onItemClicked {v->
                    onItemClicked(v)
                }.id(recipe.recipeId).addTo(this)
            }
        }
    }

}