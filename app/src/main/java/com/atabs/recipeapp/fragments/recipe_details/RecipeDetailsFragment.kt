package com.atabs.recipeapp.fragments.recipe_details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.atabs.recipeapp.fragments.recipe_list.RecipeListFragmentDirections
import com.atabs.recipeapp.uitls.plusAssign
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

class RecipeDetailsFragment : Fragment(){

    private lateinit var recipeDetailsView: RecipeDetailsView
    private val args: RecipeDetailsFragmentArgs by navArgs()
    private lateinit var viewModel: RecipeDetailsViewModel
    private lateinit var compositeDisposable: CompositeDisposable

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        recipeDetailsView = RecipeDetailsView()
        return recipeDetailsView.onCreateView(inflater, container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(RecipeDetailsViewModel::class.java)
        compositeDisposable = CompositeDisposable()
        val recipeId = args.recipeData

        if(recipeId > 0){
            compositeDisposable += viewModel.getRecipe(recipeId)
                .subscribe({
                    recipeDetailsView.setupUi(it)
                },{
                    Log.e("Error", it.localizedMessage)
                })
        }

        compositeDisposable += recipeDetailsView.getEditEvent()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                findNavController().navigate(RecipeDetailsFragmentDirections
                    .actionRecipeDetailToRecipeAdd(recipeData = recipeId))
                Log.d("Click", "click parent")
            },{
                Log.e("Error", it.localizedMessage)
            })
    }

    override fun onDestroyView() {
        if (::compositeDisposable.isInitialized &&
            !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
        super.onDestroyView()
    }
}