package com.atabs.recipeapp.fragments.add_recipe

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.view.children
import androidx.core.view.forEach
import androidx.core.view.get
import androidx.navigation.findNavController
import com.atabs.recipeapp.R
import com.atabs.recipeapp.database.data.ListStringObj
import com.atabs.recipeapp.database.data.Recipe
import com.atabs.recipeapp.databinding.FragmentAddRecipeBinding
import com.atabs.recipeapp.uitls.BitmapConverter
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.*
import kotlin.collections.HashMap


class AddRecipeView {

    private lateinit var binding: FragmentAddRecipeBinding
    private lateinit var context: Context
    private lateinit var inflater: LayoutInflater
    private val ingredientList = mutableListOf<String>()
    private val stepList = mutableListOf<String>()
    private var submitEvent = PublishSubject.create<Recipe>()
    private var addImageEvent = PublishSubject.create<Unit>()
    private var recipeId: Int = 0
    private var imageBase64 : String? = null
    private lateinit var catMapId : HashMap<String, Int>

    fun onCreateView(inflater: LayoutInflater, container: ViewGroup?) : View {
        binding = FragmentAddRecipeBinding.inflate(inflater, container, false)
        this.inflater = inflater

        return binding.root
    }

    fun displayData(recipe: Recipe){
        recipe.recipeId?.let {
            recipeId = it
        }
        binding.recipeNameEdit.setText(recipe.name)
        binding.recipeDescriptionEdit.setText(recipe.description)

        recipe.ingredientList.ingList.forEach {

            val ingredView = inflater.inflate(R.layout.component_text_button_views,
                binding.ingredientLinearLytHolder, false)
            addListToLayout(ingredView, it, binding.ingredientLinearLytHolder, ingredientList)
            //ingredientList.add(it)
        }

        recipe.stepList.ingList.forEach {
            val stepView = inflater.inflate(R.layout.component_text_button_views,
                binding.stepLinearLytHolder, false)
            addListToLayout(stepView, it, binding.stepLinearLytHolder, stepList)
           // stepList.add(it)
        }

        imageBase64 = recipe.image
    }

    fun initUi(context: Context, categories: List<String>, mapId: HashMap<String, Int>){
        this.context = context
        val adapter = ArrayAdapter<String>(context, R.layout.spinner_item, categories)

        binding.categorySpinner.adapter = adapter
        catMapId = mapId

        event()

    }

    private fun addListToLayout(view: View, text: String, viewGroup: ViewGroup, list: MutableList<String>){

        val textView = view.findViewById<TextView>(R.id.nameTv)
        val removeBtn = view.findViewById<ImageView>(R.id.removeImg)
        val map = HashMap<String, Any>()
        textView.text = text

        map["view"] = view
        map["text"] = text

        removeBtn.tag = map
        viewGroup.addView(view)
        list.add(text)
        removeBtn.setOnClickListener {
            it?.let {view->

                if(view.tag != null && view.tag is HashMap<*, *>){
                    val viewMap = view.tag as HashMap<*, *>
                    viewGroup.removeView(viewMap["view"] as View)
                    list.remove(viewMap["text"])
                }

            }
        }
    }

    private fun event(){

        binding.addIngredientBtn.setOnClickListener{

            if(binding.recipeIngredientsEditText.text.isNotEmpty()) {
                binding.noIngredientLabelTxt.visibility = View.GONE

                val ingredView = inflater.inflate(R.layout.component_text_button_views, binding.ingredientLinearLytHolder, false)
                val ingredient = binding.recipeIngredientsEditText.text

                addListToLayout(ingredView, ingredient.toString(), binding.ingredientLinearLytHolder, ingredientList)
               // ingredientList.add(ingredient.toString())
                binding.recipeIngredientsEditText.setText("")

            }

        }

        binding.addStepBtn.setOnClickListener {
            val step = binding.recipeStepEdit.text
            if(step.isNotEmpty()){
                binding.noStepLabelTxt.visibility = View.GONE

               val stepView = inflater.inflate(R.layout.component_text_button_views, binding.stepLinearLytHolder, false)
                //stepList.add(step.toString())
                addListToLayout(stepView, step.toString(), binding.stepLinearLytHolder, stepList)
                binding.recipeStepEdit.setText("")

            }

        }

        binding.submitBtn.setOnClickListener {

            if(validateField()) {

                val recipeName = binding.recipeNameEdit.text.toString()
                val recipeDescription = binding.recipeDescriptionEdit.text.toString()

                val categoryId = catMapId[binding.categorySpinner.selectedItem]?.let {
                    it
                } ?: kotlin.run {
                    1
                }

                val ingredients = ListStringObj(ingredientList)
                val steps = ListStringObj(stepList)
                var recipe = Recipe(
                    recipeCategoryId = categoryId, name = recipeName,
                    description = recipeDescription, ingredientList = ingredients, stepList = steps,
                    image = imageBase64
                )

                if (recipeId > 0) {
                    recipe = Recipe(
                        recipeId = recipeId,
                        recipeCategoryId = categoryId,
                        name = recipeName,
                        description = recipeDescription,
                        ingredientList = ingredients,
                        stepList = steps,
                        image = imageBase64
                    )
                }

                submitEvent.onNext(recipe)
            }else{
                Snackbar.make(it, "Missing information", Snackbar.LENGTH_SHORT).show()
            }

        }

        binding.addImgBtn.setOnClickListener {
            addImageEvent.onNext(Unit)
        }

    }

    private fun validateField() : Boolean{

        if(binding.recipeNameEdit.text.isEmpty()){
            return false
        }

        if(binding.recipeDescriptionEdit.text.isEmpty())
            return false

        if(ingredientList.isEmpty())
            return false

        if(stepList.isEmpty())
            return false

        if(imageBase64.isNullOrBlank())
            return false

        return true
    }

    fun getSubmitEvent() : Observable<Recipe>{
        return submitEvent
    }

    fun getAddEventImg(): Observable<Unit>{
        return addImageEvent
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun imageSelected(res: Boolean, bitmap: Bitmap){
        if(res){
            binding.imageLabelTxt.text = "Image is selected"
            imageBase64 = BitmapConverter.bitmapToString(bitmap)
        }else{

            binding.imageLabelTxt.text = "No image selected"
        }
    }

    fun imageSelected(res: Boolean, pathImg: String?){
        if(res){
            binding.imageLabelTxt.text = "Image is selected"
            imageBase64 = pathImg
        }else{
            binding.imageLabelTxt.text = "No image selected"
        }
    }
}